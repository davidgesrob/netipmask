import java.lang.*;
import java.awt.event.KeyEvent;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class NetMask implements ActionListener {

    // Toolbar 

    JToolBar toolBar = new JToolBar();
    JButton btExit = new JButton("Exit");
    JButton btRun = new JButton("Run");

    // Menubar
    
    JMenuBar menubar = new JMenuBar();
    JMenu file = new JMenu("File");
    JMenu about = new JMenu("About");

    JMenuItem cMenuExit = new JMenuItem("Exit");
    JMenuItem cMenuRun = new JMenuItem("Run");
    JMenuItem cMenuContact = new JMenuItem("Contact");

    JTextField p1 = new JTextField(3);
    JTextField p2 = new JTextField(3);
    JTextField p3 = new JTextField(3);
    JTextField p4 = new JTextField(3);
    JTextField p5 = new JTextField(2);


    JTextField u1 = new JTextField(8);
    JTextField u2 = new JTextField(8);
    JTextField u3 = new JTextField(8);
    JTextField u4 = new JTextField(8);
    JButton    u5 = new JButton("Run"); 	// Button

    JTextField q1 = new JTextField(3);
    JTextField q2 = new JTextField(3);
    JTextField q3 = new JTextField(3);
    JTextField q4 = new JTextField(3);
    JButton    q5 = new JButton("Clear"); 	// Button

    
    JTextField w1 = new JTextField(3);
    JTextField w2 = new JTextField(3);
    JTextField w3 = new JTextField(3);
    JTextField w4 = new JTextField(3);
    JButton    w5 = new JButton("Exit"); 	// Button

    JLabel     m0 = new JLabel("Dec");	 	// Label
    JTextField m1 = new JTextField(3);
    JTextField m2 = new JTextField(3);
    JTextField m3 = new JTextField(3);
    JTextField m4 = new JTextField(3);


    public static String BinToDec(String str) {
    	
    	StringBuffer store = new StringBuffer();
    	char[] ary = str.toCharArray();
    	int sum=0, parc=0, count=0;

    	for(int i = ary.length-1; i >= 0 ; i--) {
    		parc = (int)Math.pow(2,count)*Character.getNumericValue(ary[i]);
    		count++;
    		//System.out.print(parc);
    		sum = sum + parc;
    	}
    	System.out.print(parc);
    	str = Integer.toString(sum);
    	return str;

    }

    public static String DecToBin(String str) {

        int numb = Integer.parseInt(str);
        StringBuffer store = new StringBuffer();
        do {
            store.append(numb % 2);
            numb = numb / 2;
        } while (numb>0);
        str = store.reverse().toString();
        String formatted  = ("00000000"+ str).substring(str.length());
        return formatted;
    }
    public static String BitRunner(String str) {

    	int word = 32;
    	int numb = Integer.parseInt(str);
    	StringBuffer store = new StringBuffer();
    	for(int i = 0; i < word; i++) {
    		if ( i < numb ) {
    			store.append("1");
    		}
    		else {
    			store.append("0");
    		}
    	}
    	str = store.toString();
    	return str;
    }

    public static String BitMerge(String str1, String str2) {
    	
    	String str;
    	StringBuffer store = new StringBuffer();

    	char[] ary1 = str1.toCharArray();
    	char[] ary2 = str2.toCharArray();
    	
    	for (int i = 0; i<8; i++) {
    		
    		int f = Character.getNumericValue(ary1[i])*Character.getNumericValue(ary2[i]);
    		store.append(Integer.toString(f));
    	}

    	str = store.toString();

    	return str;
    }

    public void actionPerformed(ActionEvent e) {

        Runnar();        
    }

    public void Runnar() {

        u1.setText(DecToBin(p1.getText()));
        u2.setText(DecToBin(p2.getText()));
        u3.setText(DecToBin(p3.getText()));
        u4.setText(DecToBin(p4.getText()));

        q1.setText(BitRunner(p5.getText()).substring(0,8));
        q2.setText(BitRunner(p5.getText()).substring(8,16));
        q3.setText(BitRunner(p5.getText()).substring(16,24));
        q4.setText(BitRunner(p5.getText()).substring(24,32));

        w1.setText(BitMerge(u1.getText(),q1.getText()));
        w2.setText(BitMerge(u2.getText(),q2.getText()));
        w3.setText(BitMerge(u3.getText(),q3.getText()));
        w4.setText(BitMerge(u4.getText(),q4.getText()));

        m1.setText(BinToDec(w1.getText()));
        m2.setText(BinToDec(w2.getText()));
        m3.setText(BinToDec(w3.getText()));
        m4.setText(BinToDec(w4.getText()));

    }
	
	public NetMask() {

        JFrame mainw = new JFrame();
        mainw.setResizable(false);

        JPanel pan1 = new JPanel();
        pan1.setLayout(new GridLayout());

        toolBar.add(btRun);
        toolBar.add(btExit);

        file.add(cMenuRun);
        file.add(cMenuExit);

        about.add(cMenuContact);

        menubar.add(file);
        menubar.add(about);

        cMenuContact.addActionListener((ActionEvent event)-> {
            JOptionPane.showMessageDialog(null,"David Gesrob 2017-09-21\ndavid81br@gmail.com");
        });

        cMenuExit.addActionListener((ActionEvent event)->{
            System.exit(0);
        });

        cMenuRun.addActionListener((ActionEvent event)->{

            Runnar();
        });

        mainw.setJMenuBar(menubar);

        // mainw.add(toolBar, BorderLayout.NORTH);

        q1.setHorizontalAlignment(JTextField.CENTER);
        q2.setHorizontalAlignment(JTextField.CENTER);
        q3.setHorizontalAlignment(JTextField.CENTER);
        q4.setHorizontalAlignment(JTextField.CENTER);
        
        u1.setHorizontalAlignment(JTextField.CENTER);
        u2.setHorizontalAlignment(JTextField.CENTER);
        u3.setHorizontalAlignment(JTextField.CENTER);
        u4.setHorizontalAlignment(JTextField.CENTER);
        
        p1.setHorizontalAlignment(JTextField.CENTER);
        p2.setHorizontalAlignment(JTextField.CENTER);
        p3.setHorizontalAlignment(JTextField.CENTER);
        p4.setHorizontalAlignment(JTextField.CENTER);
        p5.setHorizontalAlignment(JTextField.CENTER);

        w1.setHorizontalAlignment(JTextField.CENTER);
        w2.setHorizontalAlignment(JTextField.CENTER);
        w3.setHorizontalAlignment(JTextField.CENTER);
        w4.setHorizontalAlignment(JTextField.CENTER);

        m0.setHorizontalAlignment(JTextField.CENTER);
        m1.setHorizontalAlignment(JTextField.CENTER);
        m2.setHorizontalAlignment(JTextField.CENTER);
        m3.setHorizontalAlignment(JTextField.CENTER);
        m4.setHorizontalAlignment(JTextField.CENTER);

        p1.setText("0");
        p2.setText("0");
        p3.setText("0");
        p4.setText("0");
        p5.setText("0");

        q5.addActionListener(new ActionListener() {
    		
    		@Override
    		public void actionPerformed(ActionEvent e) {
    		
    			p1.setText(null);
    			p2.setText(null);
    			p3.setText(null);
    			p4.setText(null);
    			p5.setText(null);

    			u1.setText(null);
    			u2.setText(null);
    			u3.setText(null);
    			u4.setText(null);

    			q1.setText(null);
    			q2.setText(null);
    			q3.setText(null);
    			q4.setText(null);

    			w1.setText(null);
    			w2.setText(null);
    			w3.setText(null);
    			w4.setText(null);
    			
    			m1.setText(null);
    			m2.setText(null);
    			m3.setText(null);
    			m4.setText(null);


    		}
    	});

    	w5.addActionListener(new ActionListener() {
    		
    		@Override
    		public void actionPerformed(ActionEvent e) {
    			System.exit(0);
    		}

    	});

        u5.addActionListener(this);

        // Tooltips e Teclas de atalho para funções de Botões.

        q5.setMnemonic(KeyEvent.VK_C);
        q5.setToolTipText("Limpa todos os campos.");
        u5.setMnemonic(KeyEvent.VK_R);
        u5.setToolTipText("Executa o merge entre IP e Máscara");
        w5.setMnemonic(KeyEvent.VK_X);
        w5.setToolTipText("Sai do programa");

        JLabel l1 = new JLabel("1# 8bits");
        l1.setHorizontalAlignment(JLabel.CENTER);

        JLabel l2 = new JLabel("2# 8bits");
        l2.setHorizontalAlignment(JLabel.CENTER);

        JLabel l3 = new JLabel("3# 8bits");
        l3.setHorizontalAlignment(JLabel.CENTER);

        JLabel l4 = new JLabel("4# 8bits");
        l4.setHorizontalAlignment(JLabel.CENTER);

        JLabel l5 = new JLabel("/Mask(XX)");
        l5.setHorizontalAlignment(JLabel.CENTER);


        mainw.setTitle("NetIP+/Mask v.0.1");
        mainw.setMinimumSize(new Dimension(400,100));
        mainw.setLocationRelativeTo(null);
        mainw.setLayout(new GridLayout(6,5));
        mainw.setDefaultCloseOperation(mainw.EXIT_ON_CLOSE);

        mainw.add(l1); // Label
        mainw.add(l2); // Label
        mainw.add(l3); // Label
        mainw.add(l4); // Label
        mainw.add(l5); // Label

        mainw.add(p1); // Text
        mainw.add(p2); // Text
        mainw.add(p3); // Text
        mainw.add(p4); // Text
        mainw.add(p5); // Text

        mainw.add(u1); // Calc
        mainw.add(u2);
        mainw.add(u3);
        mainw.add(u4);
        mainw.add(u5);

        mainw.add(q1); // Calc
        mainw.add(q2);
        mainw.add(q3);
        mainw.add(q4);
        mainw.add(q5);

        mainw.add(w1); // Result
        mainw.add(w2);
        mainw.add(w3);
        mainw.add(w4);
        mainw.add(w5);

        
        mainw.add(m1);
        mainw.add(m2);
        mainw.add(m3);
        mainw.add(m4);
        mainw.add(m0);

        mainw.pack();
        mainw.setVisible(true);
		
	}
    public static void main(String[] args) {
        NetMask ex = new NetMask();
    }
}
