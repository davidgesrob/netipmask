# README #

Esse pequeno projeto é originado de um exercício para cálculo de endereços de rede e máscara dos slides da disciplina "Rede de Computadores"
da Universidade Estácio de Sá - EaD - Sistemas de Informação.

### What is this repository for? ###

* Quick summary

Este software tem a função básica de realizar a operação AND com o número IP fornecido 
pelo usuário e sua respectiva máscara em notação de bits.

* Version

v.0.1

### How do I get set up? ###

* Summary of set up

Basta realizar o _download_ e executar:

```sh
javac NetMask.java
java NetMask
```

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* 
* Other community or team contact